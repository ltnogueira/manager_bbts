package br.com.bbts.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaginaMainPageElementMap extends PaginaPage{

	@FindBy(xpath = "//button[contains(.,'Acessar')]")
	public WebElement btnEntrar;

	@FindBy(id = "username")
	protected WebElement campoUsuario;

	@FindBy(id = "senha")
	protected WebElement campoSenha;
	

	@FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[1]/button")
	protected WebElement menuPrincipal;
	
	@FindBy(xpath = "//span[contains(text(),'Editais')]")
	protected WebElement subMenuEdital;
	
	@FindBy(xpath = "/html/body/div[1]/div/div[3]/div[2]/div[3]/table/tbody/tr[1]/td[6]/div/div/button")
	protected WebElement opcaoEditar;
	
	@FindBy(xpath = "//span[contains(text(),'Adesão')]")
	protected WebElement subMenuAdesao;
	

    
}
