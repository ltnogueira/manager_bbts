package br.com.bbts.pages;

import org.openqa.selenium.support.PageFactory;

import br.com.bbts.util.CustomUtils;

public class PaginaMainPage extends PaginaMainPageElementMap{

	public PaginaMainPage(){
		PageFactory.initElements(driver, this);
	}

	public void realizarLogin(String usuario, String senha){
		
		campoUsuario.sendKeys(usuario);
		campoSenha.sendKeys(senha);
	}

	public void botaoEntrar(){
		btnEntrar.click();
	}

	public void subMenus(){
		subMenuEdital.click();
	}


	public void opcaoEditar(){
		opcaoEditar.click();
	}

	//public void fecharBrowser(String usuario, String senha){
	//	campoUsuario.sendKeys(usuario);
	//	campoSenha.sendKeys(senha);
	//	btnEntrar.click();
	//}

	public void validarDetalhePagina(String arg1) throws Exception {
		CustomUtils.paginaContemTexto(arg1);
	}

	public void clicarMenuPrincipal() throws Exception {
		menuPrincipal.click();
	}



	/* public boolean isPesquisaRetornouResultados(){
	        return !lblQtdResultadosEncontrados.getText().contains("0");
	    }*/
}
