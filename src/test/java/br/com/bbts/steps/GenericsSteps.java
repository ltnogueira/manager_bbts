package br.com.bbts.steps;

import org.junit.Test;

import br.com.bbts.pages.PaginaMainPage;
import br.com.bbts.pages.PaginaMainPageElementMap;
import br.com.bbts.util.CustomUtils;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;

public class GenericsSteps extends CustomUtils {

	PaginaMainPage paginaMainPage = new PaginaMainPage();
	PaginaMainPageElementMap pg = new PaginaMainPageElementMap();
	@Test
	@Dado("^que acesso o sistema$")
	public void queAcessoOSistema() throws Exception {
		String pnldUrl = "https://contracheque.bbts.com.br/";
		CustomUtils.abrirNavegador(pnldUrl);
	}
	@Test
	@E("^preencho o campo Usuario \"([^\"]*)\" e senha \"([^\"]*)\"$")
	public void preenchoOCampoUsuarioESenha(String usuario, String senha) throws Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.realizarLogin(usuario, senha);
	}
	@Test
	@E("^clico no botao acessar$")
	public void clicoNoBotaoAcessar() throws  Exception {
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		capturarTela("Screenshors");
		paginaMainPage.botaoEntrar();
	}
	@Test
	@Entao("^sistema apresenta o seguinte texto \"([^\"]*)\"$")
	public void sistemaApresentaOSeguinteTexto(String arg1) throws Exception {
		Thread.sleep(3000);
		PaginaMainPage paginaMainPage = new PaginaMainPage();
		paginaMainPage.validarDetalhePagina(arg1);
		capturarTela(arg1);
    }

}
