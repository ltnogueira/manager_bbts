package br.com.bbts.steps;

import org.openqa.selenium.WebDriver;

import br.com.bbts.util.CustomUtils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hooks extends CustomUtils{

	private static WebDriver driver;
	@Before
	public void beforeScenario(Scenario scenario){
		
	}
	void capturarTela() {
	}
	
	@After
	public void afterScenario(){
		if(driver != null){
			capturarTela();
			driver.close();
			driver.quit();
			driver = null;
		}
	}
}
