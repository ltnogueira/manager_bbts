package br.com.bbts.runners;

import org.junit.ClassRule;
import org.junit.runner.RunWith;

import br.com.bbts.util.CustomUtils;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

	@RunWith(Cucumber.class)
	@CucumberOptions(
			plugin = {"pretty","html:target/cucumber-reports/cucumber.html"},
			features = "classpath:features",
			tags = "@FazerLogin",
			glue = {"br.com.bbts.steps"}
			)
	public class RealizarLoginRunner {

		@ClassRule
		public static CustomUtils customultis = new CustomUtils();

	}

